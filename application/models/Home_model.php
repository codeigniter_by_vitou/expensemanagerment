<?php

class home_model extends CI_Model{

  function get_expense_and_income()
  {
    $this->db->select('cate.cid as cate_id, cate.name as cate_name,act.*');
    $this->db->from('categories as cate');
    $this->db->join('activities as act',' cate.cid = act.cid');
    // $this->db->select('*');
    // $this->db->from('activities');
    $query = $this->db->get();

    return $query->result();
  }
  function get_category()
  {
    $this->db->select('*');
    $this->db->from('categories');
    $query = $this->db->get();

    return $query->result();
  }
  function sum_income()
  {
    $where = "date_format(add_date,'%Y-%m-%d') = '".date('Y-m-d')."'";
    $this->db->select_sum('amount');
    $this->db->from('activities');
    $this->db->Where($where)->where("(type ='1')");
    $query = $this->db->get();
    return $query->result();
  }

  function sum_expense()
  {
    $where = "date_format(add_date,'%Y-%m-%d') = '".date('Y-m-d')."'";
    $this->db->select_sum('amount');
    $this->db->from('activities');
    $this->db->Where($where)->where("(type ='2')");
    $query = $this->db->get();
    return $query->result();
  }

  // this function is used to filter current weekly
  function this_weekly_income()
  {
    $where = "yearweek(add_date,1) = yearweek(curdate(),1)";
    $this->db->select_sum('amount');
    $this->db->from('activities');
    $this->db->Where($where)->where("(type ='1')");
    $query = $this->db->get();
    return $query->result();
  }
  // this function is used to filter current weekly
  function this_weekly_expense()
  {
    $where = "yearweek(add_date,1) = yearweek(curdate(),1)";
    $this->db->select_sum('amount');
    $this->db->from('activities');
    $this->db->Where($where)->where("(type ='2')");
    $query = $this->db->get();
    return $query->result();
  }

  // this function is used to filter current weekly
  function this_monthly_income()
  {
    $where = "month(add_date)=month(curdate()) and year(add_date)= year(curdate())";
    $this->db->select_sum('amount');
    $this->db->from('activities');
    $this->db->Where($where)->where("(type ='1')");
    $query = $this->db->get();
    return $query->result();
  }
  // this function is used to filter current weekly
  function this_monthly_expense()
  {
    $where = "month(add_date)=month(curdate()) and year(add_date)= year(curdate())";
    $this->db->select_sum('amount');
    $this->db->from('activities');
    $this->db->Where($where)->where("(type ='2')");
    $query = $this->db->get();
    return $query->result();
  }
  // this function is used to filter current weekly
  function this_yearly_income()
  {
    $where = "year(add_date)= year(curdate())";
    $this->db->select_sum('amount');
    $this->db->from('activities');
    $this->db->Where($where)->where("(type ='1')");
    $query = $this->db->get();
    return $query->result();
  }
  // this function is used to filter current weekly
  function this_yearly_expense()
  {
    $where = "year(add_date)= year(curdate())";
    $this->db->select_sum('amount');
    $this->db->from('activities');
    $this->db->Where($where)->where("(type ='2')");
    $query = $this->db->get();
    return $query->result();
  }
  // activity_udpate
  function activity_udpate($id,$data)
  {
    // print_r($id);exit();
    $this->db->where('act_id',$id);
    $this->db->update('activities',$data);
  }
  function view($id)
  {
    $this->db->select('cate.cid as cate_id, cate.name as cate_name,act.*');
    $this->db->from('categories as cate,activities as act');
    $this->db->join('activities as act',' cate.cid = act.cid');
    // $this->db->where('act_id',$id);
    // $q = $this->db->get('activities');

    return $q->result();
  }
}
  ?>
