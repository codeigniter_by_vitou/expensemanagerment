<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  class expensecontroller extends CI_Controller{

    public function __construct()
    {
      parent::__construct();
      $this->load->model('home_model');
      $this->load->model('CategoryModel','category');
      $this->load->model('ExpenseModel','expense');
    }
    function index()
    {
      $this->load->view('home');
    }
    function store()
    {
      $type = $this->input->post('type');
      if ($type =='1') {
        $type_name = 'income(+)';
      }else{
        $type_name = 'expense(-)';
      }
      $data =array(
        'amount' =>$this->input->post('amount'),
        'type' =>$type,
        'type_name' =>$type_name,
        'cid' =>$this->input->post('category'),
        'add_date'=>date('Y-m-d - H:i'),
        'last_modified'=>date('Y-m-d - H:i'),
      );
      if ($data) {
          $this->expense->store($data);
          $this->session->set_flashdata('msg','congrate');
      }
      redirect (base_url('index.php/homecontroller'));
    }
  }

 ?>
