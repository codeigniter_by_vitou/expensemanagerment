<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  class CategoryController extends CI_Controller{

    public function __construct()
    {
      parent::__construct();
      $this->load->model('home_model');
      $this->load->model('CategoryModel','category');
    }
    function index()
    {
      $this->load->view('home');
    }
    function store()
    {
      // print_r('ok');exit();
      $data =array(
        'name' =>$this->input->post('category_name'),
        'add_date'=>date('Y-m-d - H:i:s'),
        'last_modified'=>date('Y-m-d - H:i:s'),
      );
      if (!empty($this->input->post('category_name'))) {
          $this->category->store($data);
          $this->session->set_flashdata('msg','congrate');
      }
      redirect (base_url('index.php/homecontroller'));
    }
  }

 ?>
