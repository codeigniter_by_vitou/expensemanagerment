<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  class HomeController extends CI_Controller{

    public function __construct()
    {
      parent::__construct();
      $this->load->model('home_model','home');
      $this->load->model('CategoryModel','category');
    }
    function index()
    {
      // print_r($this->home->get_expense_and_income());exit();

      $data['get_expense_and_income'] = $this->home->get_expense_and_income();
      $data['categories'] = $this->home->get_category();
      $this->load->view('home',$data);
    }

    function overview()
    {
      $data['income'] = $this->home->sum_income();
      $data['sum_expense'] = $this->home->sum_expense();
      $data['categories'] = $this->home->get_category();
      $this->load->view('overview/overview_index',$data);
    }
    // This function is used to delete entry
    function delete($id)
    {
      if ($id != null) {
          $this->db->where('act_id',$id);
          $this->db->delete('activities');
          $this->session->set_flashdata('msg','Congratulation');
      }
      return(redirect(base_url('index.php/homecontroller')));
    }

    // this fucntion is used to filter this Weekly
    function this_weekly()
    {
      $data['income'] = $this->home->this_weekly_income();
      $data['sum_expense'] = $this->home->this_weekly_expense();
      $data['categories'] = $this->home->get_category();
      $this->load->view('overview/overview_index',$data);
    }

    // this fucntion is used to filter this Weekly
    function this_monthly()
    {
      $data['income'] = $this->home->this_monthly_income();
      $data['sum_expense'] = $this->home->this_monthly_expense();
      $data['categories'] = $this->home->get_category();
      $this->load->view('overview/overview_index',$data);
    }
    // this fucntion is used to filter this Weekly
    function this_yearly()
    {
      $data['income'] = $this->home->this_yearly_income();
      $data['sum_expense'] = $this->home->this_yearly_expense();
      $data['categories'] = $this->home->get_category();
      $this->load->view('overview/overview_index',$data);
    }

    function update()
    {
      $id = $this->input->post('act_id');
      $type = $this->input->post('type');
      if ($type =='1') {
        $type_name = 'income(+)';
      }else{
        $type_name = 'expense(-)';
      }
      $data =array(
        'amount' =>$this->input->post('amount'),
        'type' =>$type,
        'type_name' =>$type_name,
        'cid' =>$this->input->post('category'),
        'add_date'=>date('Y-m-d - H:i'),
        'last_modified'=>date('Y-m-d - H:i'),
      );
      if ($id != null) {
          $this->home->activity_udpate($id,$data);
          $this->session->set_flashdata('msg','Congratulation');
      }
      return(redirect(base_url('index.php/homecontroller')));
    }

    function view($id)
    {
      if ($id!=null) {
        $data['get_expense_and_income'] = $this->home->view($id);
        $data['categories'] = $this->home->get_category();
        $this->load->view('home',$data);
      }else{
        redirect(base_url('homecontroller'));
      }

    }


  }

 ?>
