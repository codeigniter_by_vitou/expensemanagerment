
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../../../../favicon.ico">

  <title>Blog Template for Bootstrap</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/fontawesome/css/all.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/custom/css/custom.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>assets/jquery-form-validator/theme-default.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>assets/select2/css/select2.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
</head>
<body>
  <div class="container">
    <header class="blog-header py-3">
      <div class="row flex-nowrap align-items-center">
        <div class="col-4 pt-1">
          <a class="text-muted" href="<?php echo base_url().'index.php/homecontroller'; ?>"><strong>Expense Management</strong></a>
        </div>
        <div class="col">
          <a class="px-2" href="<?php echo base_url().'index.php/homecontroller'; ?>">Home</a>
          <a class="px-2" href="#" data-toggle="modal" data-target="#NewExpense">New Expense</a>
          <a class="px-2" href="#" data-toggle="modal" data-target="#Category">New Category</a>
          <a class="px-2" href="<?php echo base_url('index.php/homecontroller/overview'); ?>">Overview</a>
        </div>
      </div>
    </header>
    <div class="row my-2">
      <div class="col-6">
        <a class="px-2 text-muted" href="<?php echo base_url('index.php/homecontroller/overview') ;?>">Today</a>
        <a class="px-2 text-muted" href="<?php echo base_url().'index.php/homecontroller/this_weekly'; ?>">Weekly</a>
        <a class="px-2 text-muted" href="<?php echo base_url().'index.php/homecontroller/this_monthly'; ?>">Monthly</a>
        <a class="px-2 text-muted" href="<?php echo base_url().'index.php/homecontroller/this_yearly'; ?>">Yearly</a>
      </div>
      <div class="col-6">
        <a class="px-2 float-right btn btn-primary " href="#" data-toggle="modal" data-target="#NewExpense">Add new</a>
        <?php if(!empty($this->session->flashdata('msg'))){ ?>
          <a class="px-2 py-2 my-0 float-left alert alert-success " href="#"><i class="px-1 fas fa-thumbs-up"></i>Congratulation!</a>
        <?php } ?>
      </div>
    </div>
    <div class="row my-2">
      <div class="col">
        <table class="table table-bordered text-center">
          <thead>
            <tr>
              <th scope="col">Income(+)</th>
              <th scope="col">Expense(-)</th>
              <th scope="col">Belance</th>
            </tr>
          </thead>
          <tbody >
            <tr>
              <td class=" text-center">
                <?php foreach ($income as $row): ?>
                  <span>&#36;&nbsp;</span>
                  <?php if(!empty($row->amount)): ?>
                    <?= $row->amount ?>
                  <?php else: ?>
                    0
                  <?php endif; ?>
                <?php endforeach; ?>
              </td>
              <td>
                <?php foreach ($sum_expense as $row): ?>
                  <span>&#36;&nbsp;</span>
                  <?php if(!empty($row->amount)): ?>
                    <?= $row->amount ?>
                  <?php else: ?>
                    0
                  <?php endif; ?>
                <?php endforeach; ?>
              </td>
              <td >
                <?php foreach ($income as $income_row):?>
                  <?php foreach ($sum_expense as $sum_expense_row):?>
                    &#36;&nbsp;<?= $income_row->amount - $sum_expense_row->amount  ?>
                  <?php endforeach; ?>
                <?php endforeach; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- modal -->
  <div class="modal fade" id="Category" tabindex="-1" role="dialog" aria-labelledby="CategoryLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="CategoryLabel">New Category</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?php echo base_url().'index.php/categorycontroller/store';?>" method="POST" >
          <div class="modal-body">
            <div class="form-group">
              <label for="category_name" class="col-form-label">Name:</label>
              <input type="text" name="category_name" placeholder="Enter value" class="form-control" id="category_name" value="<?php echo set_value('category_name'); ?>" data-validation="length alphanumeric" data-validation-length="min2">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- New expense -->
  <div class="modal fade" id="NewExpense" tabindex="-1" role="dialog" aria-labelledby="CategoryLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="CategoryLabel">New Expense</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?php echo base_url().'index.php/expensecontroller/store';?>" method="POST" id="NewExpense_form">
          <div class="modal-body">
            <div class="form-group">
              <label for="amount" class="col-form-label">Amount<span>*</span>:</label>
              <input type="text" name="amount" placeholder="$0.00" class="form-control" id="amount" value="<?php echo set_value('amount'); ?>" data-validation="length number" data-validation-length="min1" data-validation-allowing="float,$">
            </div>
            <div class="form-group">
              <label for=" type" class="col-form-lable">Type<span>*</span></label>
              <select class="form-control" name="type">
                <option value="1" <?php echo set_select('type','1'); ?>>Income(+)</option>
                <option value="2" <?php echo set_select('type','2'); ?>>Expense(-)</option>
              </select>
            </div>
            <div class="form-group">
              <label for="category" class="col-form-lable">Category<span>*</span></label>
              <select class="form-control" name="category">
                <?php foreach ($categories as $row) {?>
                  <option value="<?php echo $row->cid ;?>"><?php echo $row->name; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <!-- script -->
  <script src="<?php echo base_url(); ?>assets/jquery/jquery.min.js" charset="utf-8"></script>
  <script src="<?php echo base_url(); ?>assets/jquery-form-validator/jquery.form-validator.min.js" charset="utf-8"></script>
  <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
  <script src="<?php echo base_url(); ?>assets/select2/js/select2.full.js" charset="utf-8"></script>

  <script type="text/javascript">
  $('#Category').on('show.bs.modal', function (event) {
    // var button = $(event.relatedTarget) // Button that triggered the modal
    // var recipient = button.data('whatever') // Extract info from data-* attributes
    // // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    // var modal = $(this)
    // modal.find('.modal-title').text('New message to ' + recipient)
    // modal.find('.modal-body input').val(recipient)
  })
  $('#NewExpense').on('show.bs.modal', function (event) {
    // var button = $(event.relatedTarget) // Button that triggered the modal
    // var recipient = button.data('whatever') // Extract info from data-* attributes
    // // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    // var modal = $(this)
    // modal.find('.modal-title').text('New message to ' + recipient)
    // modal.find('.modal-body input').val(recipient)
  })

  // validator
  $.validate();

  // select2
  // $(document).ready(function() {
  //     $('.js-example-basic-single').select2({ width: '100%' });
  //     $(".aa").select2();
  // });

  // $("#aa").select2();

  </script>
</body>
</html>
