-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 28, 2018 at 10:01 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `expense_managerment`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
CREATE TABLE IF NOT EXISTS `activities` (
  `act_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(250) NOT NULL DEFAULT '0' COMMENT '$0.00',
  `type` int(250) NOT NULL COMMENT '1(income) /2(expense)',
  `type_name` varchar(260) NOT NULL,
  `cid` int(11) NOT NULL,
  `add_date` date NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`act_id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`act_id`, `amount`, `type`, `type_name`, `cid`, `add_date`, `last_modified`) VALUES
(33, 111, 2, 'expense(-)', 1, '2018-09-28', '2018-09-28 16:56:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `add_date` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cid`, `name`, `add_date`, `last_modified`) VALUES
(1, 'moto', '2018-09-27 22:32:19', '2018-09-27 22:32:19'),
(2, 'car', '2018-09-28 00:00:00', '2018-09-28 00:00:00'),
(3, 'clothes', '2018-09-28 09:34:58', '2018-09-28 09:34:58'),
(4, 'clothes', '2018-09-28 09:35:30', '2018-09-28 09:35:30'),
(5, 'moto', '2018-09-28 11:24:46', '2018-09-28 11:24:46');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
